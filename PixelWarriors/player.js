var player = {
	x:320,
	y:10,
	width:24,
	height:96,
	xvel:0,
	yvel:0,
	xacc:0,
	yacc:0.5,
	movingLeft:false,
	movingRight:false,
	facingRight:true,
	falling:true,
	type: "archer",
	maxYvel:8,
	jumping:true,
	jumpVel:-12,
	doubleJump:true,
	doubleJumpVel:-8,
	defaultYacc:0.5,
	username:"",
	jump:function(){
		if(!player.jumping){
			player.jumping = true;
			player.yvel=player.jumpVel;
			player.yacc=player.defaultYacc;
		}else if(player.doubleJump && player.yvel > player.doubleJumpVel){
			player.jumping = true;
			player.doubleJump = false;
			player.yvel=player.doubleJumpVel;
			player.yacc=player.defaultYacc;
		}
	},
	hitting:function(platform){
		player.x+=player.xvel;
		player.y+=player.yvel;
		var retval = 0;
		if(player.x >= platform.tlx-player.width/2 && player.x <= platform.brx+player.width/2 && player.y <= platform.bry+player.height/2 && player.y >= platform.tly-player.height/2){
			player.x-=player.xvel;
			player.y-=player.yvel;
			//On top of platform
			if(player.x + player.width/2 >= platform.tlx && player.x - player.width/2+1 <= platform.brx){
				if(player.y <= platform.tly){
					retval = 1;//"TOP"
				}else if(player.y >= platform.bry){
					retval = 2;//"BOTTOM"
				}
			}
			if(player.y + player.height/2 >= platform.tly && player.y - player.height/2 <= platform.bry ){
				if(player.x + player.width/2 <= platform.tlx){
					retval = 3;//"LEFT"
				}else if(player.x - player.width/2 >= platform.brx){
					retval = 4;//"RIGHT"
				}
			}
		}else{
			player.x-=player.xvel;
			player.y-=player.yvel;
		}
		return retval;
	},
	hittingPlayer:function(otherPlayer){
		player.x+=player.xvel;
		player.y+=player.yvel;
		var retval = 0;
		var tlx = otherPlayer.x-otherPlayer.width/2;
		var tly = otherPlayer.y-otherPlayer.height/2;
		var brx = otherPlayer.x+otherPlayer.width/2;
		var bry = otherPlayer.y+otherPlayer.height/2;
		if(player.x >= tlx-player.width/2 && player.x <= brx+player.width/2 && player.y <= bry+player.height/2 && player.y >= tly-player.height/2){
			player.x-=player.xvel;
			player.y-=player.yvel;
			//On top of platform
			if(player.x + player.width/2 >= tlx && player.x - player.width/2+1 <= brx){
				if(player.y <= tly){
					retval = 1;//"TOP"
				}else if(player.y >= bry){
					retval = 2;//"BOTTOM"
				}
			}
			if(player.y + player.height/2 >= tly && player.y - player.height/2 <= bry ){
				if(player.x + player.width/2 <= tlx){
					retval = 3;//"LEFT"
				}else if(player.x - player.width/2 >= brx){
					retval = 4;//"RIGHT"
				}
			}
		}else{
			player.x-=player.xvel;
			player.y-=player.yvel;
		}
		return retval;
	}
};