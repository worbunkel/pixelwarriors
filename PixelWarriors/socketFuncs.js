var socket = io();
var switchToNewUser = function(){
	debug("switchToNewUser,socketFuncs.js");
	document.getElementById('authForm').style.display = 'none';
	document.getElementById('newUserForm').style.display = 'block';
	document.getElementById('switchToNewUser').style.display = 'none';
	document.getElementById('switchToLogin').style.display = 'block';
	document.getElementById('mainHeader').innerHTML = "Create User";
};
var switchToLogin = function(){
	debug("switchToLogin,socketFuncs.js");
	document.getElementById('authForm').style.display = 'block';
	document.getElementById('newUserForm').style.display = 'none';
	document.getElementById('switchToNewUser').style.display = 'block';
	document.getElementById('switchToLogin').style.display = 'none';
	document.getElementById('mainHeader').innerHTML = "Login";
};
$('form#auth').submit(function(){
	debug("form#auth,socketFuncs.js");
	if($('input#username').val() != "" && $('input#password').val() != ""){
		socket.emit('login', $('input#username').val(),$('input#password').val());
		document.getElementById('authForm').style.display = "none";
	}
	return false;
});
$('form#newUser').submit(function(){
	debug("form#newUser,socketFuncs.js");
	if($('input#NUusername').val() != "" && $('input#NUpassword').val() != "" && $('input#NUconfirmation').val() != "" && $('input#NUpassword').val() == $('input#NUconfirmation').val()){
		document.getElementById('newUserForm').style.display = "none";
		socket.emit('newUser', $('input#NUusername').val(),$('input#NUpassword').val());
	}else{
		if($('input#NUpassword').val() != $('input#NUconfirmation').val()){
			document.getElementById('NUerror').innerHTML = "Make sure Password and Password Confirmation match.";
			document.getElementById('NUpassword').value = "";
			document.getElementById('NUconfirmation').value = "";
		}
	}
	return false;
});
socket.on('newUser',function(error){
	debug("newUser,socketFuncs.js");
	if(error){
		document.getElementById('newUserForm').style.display = "block";
		document.getElementById('NUerror').innerHTML = "That username is already taken.";
	}else{
		socket.emit('login', $('input#NUusername').val(),$('input#NUpassword').val());
	}
});

socket.on('login', function(error,un){
	debug("login,socketFuncs.js");
	/*INCORRECT LOGIN INFO*/
	if(error == 1 ){
		document.getElementById('authForm').style.display = "block";
		document.getElementById('error').innerHTML = "That username/password combination is invalid.";
	}
	/*USER ALREADY LOGGED IN*/
	else if(error == 2){
		document.getElementById('authForm').style.display = "block";
		document.getElementById('error').innerHTML = "That user is already logged in elsewhere, please log off first.";
	}else{			
		player.username=un;
		document.getElementById('canvas').style.display = "block";
		document.getElementById('switchToLogin').style.display = "none";
		document.getElementById('switchToNewUser').style.display = "none";
		document.getElementById('mainHeader').innerHTML = "Pixel Warriors";
		canvas.focus();
	}
});

socket.on('otherPlayer', function(otherPlayer){
	debug("otherPlayer,socketFuncs.js");
	for(var i = 0; i < players.length; i++){
		if(players[i].username == otherPlayer.username){
			players[i]=otherPlayer;
			return;
		}
	}
	players.push(otherPlayer);
});

socket.on('disconnectPlayer', function(un){
	debug("disconnectPlayer,socketFuncs.js");
	for(var i = 0; i < players.length; i++){
		if(players[i].username.toLowerCase() == un.toLowerCase()){
			players.splice(i,1);
		}
	}
});