var loadMap = function(mapString){
	if(mapString == "map1"){
		debug("Map 1 loaded, maps.js");
		return map1;
	}
}

var map1 = {
	platforms : [
		//Bounding Platforms
		{tlx:-10,tly:0,brx:0,bry:1000},
		{tlx:0,tly:1000,brx:1500,bry:1010},
		{tlx:1500,tly:0,brx:1510,bry:1000},
		{tlx:0,tly:-10,brx:1500,bry:0},
		//Other Platforms
		{tlx:0,tly:150,brx:500,bry:175},
		{tlx:0,tly:325,brx:500,bry:350},
		{tlx:0,tly:500,brx:500,bry:525},
		{tlx:0,tly:675,brx:500,bry:700},
		{tlx:0,tly:850,brx:500,bry:875}
		],
	width: 1500,
	height: 1000
	};


