var express = require('express');
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var fs = require('fs');
var crypto = require("crypto-js");
var AES = require("crypto-js/aes");
var debugMode = false;
var debug = function(string){
	if(debugMode){
		console.log(string);
	}
};
//users[0] = usernames
//users[1] = passwords
//users[2] = current users
//users[3] = sockets
var users = [[],[],[],[]];
fs.readFile('PixelWarriors/data', 'utf8', function (err, data) {
	debug("readFile,index.js");
	if (err) {
		console.log(err);
	}
	data = data.split(",");
	for(var i = 0; i < data.length; i++){
		if(data[i] != ""){
			if(i%2 == 0){
				users[0].push(AES.decrypt(data[i], "LukesPassphrase").toString(crypto.enc.Utf8));
				debug("USER:" + AES.decrypt(data[i], "LukesPassphrase").toString(crypto.enc.Utf8));
			}else{
				users[1].push(AES.decrypt(data[i], "LukesPassphrase").toString(crypto.enc.Utf8));
				debug("PW:" + AES.decrypt(data[i], "LukesPassphrase").toString(crypto.enc.Utf8));
			}
		}
	}
});
var socket1 = null;
var socket2 = null;
var count = 0;
var started = false;
var interval;
var player1,player2;
var grid = [];
var viewers = [];
var players = [];

app.get('/', function(req, res){
	debug("/,index.js");
    res.sendfile('PixelWarriors/index.html');
});
app.get('/music', function(req, res){
	debug("music,index.js");
    res.sendfile('PixelWarriors/Electric-AlaShook-10438_hifi.mp3');
});
app.get('/css', function(req, res){
	debug("css,index.js");
    res.sendfile('PixelWarriors/main.css');
});
app.get('/src/game', function(req, res) {
	debug("game,index.js");
	res.sendfile('PixelWarriors/game.js');
});
app.get('/src/canvas', function(req, res) {
	debug("canvas,index.js");
	res.sendfile('PixelWarriors/canvas.js');
});
app.get('/src/socketFuncs', function(req, res) {
	debug("socketFuncs,index.js");
	res.sendfile('PixelWarriors/socketFuncs.js');
});
app.get('/src/player', function(req, res) {
	debug("player,index.js");
	res.sendfile('PixelWarriors/player.js');
});
app.get('/src/images', function(req, res) {
	debug("images,index.js");
	res.sendfile('PixelWarriors/images.js');
});
app.get('/src/maps', function(req, res) {
	debug("maps,index.js");
	res.sendfile('PixelWarriors/maps.js');
});
app.get('/images/archerRight', function(req, res) {
	res.sendfile('PixelWarriors/Images/8xarcher.png');
	debug("archerRight,index.js");
});
app.get('/images/archerLeft', function(req, res) {
	res.sendfile('PixelWarriors/Images/8xarcherleft.png');
	debug("archerLeft,index.js");
});

io.on('connection', function(socket){
	debug("connection,index.js");
	socket.on('login', function(username,password){
		debug("login,index.js");
		unIndex = users[0].indexOf(username.toLowerCase());
		console.log(unIndex);
		console.log(users[2].indexOf(username.toLowerCase()));
		if(unIndex != -1 && password == users[1][unIndex] && users[2].indexOf(username.toLowerCase()) == -1){
			socket.emit('login',0,username);
			users[2].push(username.toLowerCase());
			users[3].push(socket);
		}else{
			if(users[2].indexOf(username.toLowerCase()) != -1){
				socket.emit('login',2,null);
			}else{
				socket.emit('login',1,null);		
			}
			console.log(users);
		}
	});
	socket.on('newUser', function(username,password){
		debug("newUser,index.js");
		if(users[0].indexOf(username.toLowerCase()) == -1){
			users[0].push(username.toLowerCase());
			users[1].push(password);
			socket.emit('newUser',false);
			fs.appendFile('PixelWarriors/data', AES.encrypt(username.toLowerCase(), "LukesPassphrase")+","+AES.encrypt(password, "LukesPassphrase")+",", function (err) {
				if (err){
					console.log(err);
				}else{
					console.log("New user data added");
				}
			});
		}else{
			socket.emit('newUser',true);
		}
	});
	socket.on('update', function(player){
		debug("update,index.js");
		socket.broadcast.emit('otherPlayer',player);
	});
	socket.on('disconnect', function () {
		debug("disconnect,index.js");
		index = users[3].indexOf(socket);
		socket.broadcast.emit('disconnectPlayer',users[2][index]);
		if (index > -1) {
			users[2].splice(index, 1);
			users[3].splice(index, 1);
		}
	});
});

http.listen(80, function(){
  console.log('listening on *:80');
});
