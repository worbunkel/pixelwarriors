var canvas = document.getElementById("myCanvas");
canvas.addEventListener('keydown', function(e) {onKeyDown(e);}, true);
canvas.addEventListener('keyup', function(e) {onKeyUp(e);}, true);
canvas.addEventListener('click', function(e) {onClick(e);}, false);
canvas.oncontextmenu = function (e) {
	e.preventDefault();
	onClick(e);
};
var ctx = canvas.getContext("2d");
var interval;
canvas.onblur = function () {
    socket.emit('pause');
	clearInterval(interval);
	debug('disconnected');
	audioLoop.pause();
	audioLoop.currentTime = 0;
};

canvas.onfocus = function () {
	init();
	debug('connected');
};