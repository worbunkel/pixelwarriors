var platforms = [];
var map;
var lastTime = (new Date)*1;
var players = [];
var audioLoop;
var framerate = 0;
var cameraOffsetX = 0;
var cameraOffsetY = 0;
function onClick(e){
	var left, right;
	left = 0;
	middle = 1;
	right = 2;
	var mouse = {
		x: e.pageX - canvasPosition.x,
		y: e.pageY - canvasPosition.y
	};
};
function onKeyDown(e){
	//w
	if ( e.keyCode == 87 ) {
		player.jump();
	}
	//s
	if ( e.keyCode == 83 ) {
		
	}
	//a
	if ( e.keyCode == 65 ) {
		player.movingLeft = true;
	}
	//d
	if ( e.keyCode == 68 ) {
		player.movingRight = true;					
	}
	//q
	if ( e.keyCode == 81 ) {
	
	}
};
function onKeyUp(e){
	//w
	if ( e.keyCode == 87 ) {
	}
	//s
	if ( e.keyCode == 83 ) {
		
	}
	//a
	if ( e.keyCode == 65 ) {
		player.movingLeft = false;					
	}
	//d
	if ( e.keyCode == 68 ) {
		player.movingRight = false;					
	}
	//q
	if ( e.keyCode == 81 ) {
	
	}
};

function update(){
	debug("begin update,game.js");
	player.yvel += player.yacc;
	player.xvel += player.xacc;
	if(player.yvel >= player.maxYvel){
		player.yvel = player.maxYvel;
	}
	if(player.movingLeft && player.movingRight){
		player.xvel = 0;
	}else if(player.movingLeft){
		player.xvel = -8;
	}else if(player.movingRight){
		player.xvel = 8;
	}else{
		player.xvel = 0;
	}
	player.falling = true;
	for(var i = 0; i < platforms.length; i++){
		var hitPlatform = player.hitting(platforms[i]);
		if(hitPlatform>0){
			if(hitPlatform == 1){
				player.yacc = 0;
				player.yvel = 0;
				player.jumping = false;
				player.doubleJump = true;
				player.y = platforms[i].tly - player.height/2 - 1;
			}if(hitPlatform == 2){
				player.yvel = 0;
				player.y = platforms[i].bry + player.height/2 + 1;
			}if(hitPlatform == 3){
				player.xvel = 0;
				player.x = platforms[i].tlx - player.width/2 - 1;
			}if(hitPlatform == 4){
				player.xvel = 0;
				player.x = platforms[i].brx + player.width/2 + 1;
			}
		}
		if(player.y+player.height/2+1 == platforms[i].tly && player.x>= platforms[i].tlx-player.width/2 && player.x <= platforms[i].brx+player.width/2){
			player.falling = false;
		}
	}
	for(var i = 0; i < players.length; i++){
		var hitPlayer = player.hittingPlayer(players[i]);
		if(hitPlayer>0){
			if(hitPlayer == 1){
				player.yacc = 0;
				player.yvel = 0;
				player.jumping = false;
				player.doubleJump = true;
				player.y = players[i].y-players[i].height/2 - player.height/2 - 1;
			}if(hitPlayer == 2){
				player.yvel = 0;
				player.y = players[i].y+players[i].height/2 + player.height/2 + 1;
			}if(hitPlayer == 3){
				player.xvel = 0;
				player.x = players[i].x-players[i].width/2 - player.width/2 - 1;
			}if(hitPlayer == 4){
				player.xvel = 0;
				player.x = players[i].x+players[i].width/2+ player.width/2 + 1;
			}
		}
		if(player.y+player.height/2 == players[i].y-players[i].height/2 - 1 && player.x>= players[i].x-players[i].width/2-player.width/2 && player.x <= players[i].x+players[i].width/2+player.width/2){
			player.falling = false;
		}
	}
	player.x += player.xvel;
	player.y += player.yvel;
	if(player.falling){
		player.jumping = true;
	}
	if(player.jumping){
		player.yacc = 0.5;
	}
	if(player.xvel>0){
		player.facingRight = true;
	}else if(player.xvel<0){
		player.facingRight = false;
	}
	cameraOffsetX = (map.width-canvas.width)*((player.x-canvas.width/4)/(map.width-canvas.width/4));
	cameraOffsetY = (map.height-canvas.height)*((player.y-canvas.height/4)/(map.height-canvas.height/2));
	if(cameraOffsetX < 0){
		cameraOffsetX = 0;
	}else if(cameraOffsetX > map.width-canvas.width){
		cameraOffsetX = map.width-canvas.width;
	}
	if(cameraOffsetY < 0){
		cameraOffsetY = 0;
	}else if(cameraOffsetY > map.height-canvas.height){
		cameraOffsetY = map.height-canvas.height;
	}
	draw();
	socket.emit("update",player);
	debug("end update,game.js");
};

function draw(){
	framerate = 1000/((new Date)*1-lastTime);
	lastTime = (new Date)*1;
	ctx.clearRect(0, 0, canvas.width, canvas.height);
	for(var i = 0; i < platforms.length; i++){
		ctx.fillStyle = '#888';
		ctx.beginPath();
		ctx.rect(platforms[i].tlx-cameraOffsetX,platforms[i].tly-cameraOffsetY,platforms[i].brx-platforms[i].tlx,platforms[i].bry-platforms[i].tly);
		ctx.closePath();
		ctx.fill();
	}
	drawPlayer(player);
	for(var i = 0; i < players.length; i++){
		drawPlayer(players[i]);
	}
};

function drawPlayer(player){
	ctx.beginPath();
	if(player.type == "archer"){
		if(player.facingRight){
			ctx.drawImage(archerRight.img, player.x-archerRight.width/2-cameraOffsetX, player.y-archerRight.height/2-cameraOffsetY);
		}else{
			ctx.drawImage(archerLeft.img, player.x-archerLeft.width/2-cameraOffsetX, player.y-archerLeft.height/2-cameraOffsetY);
		}
	}
	ctx.closePath();
	ctx.stroke();
	ctx.font="10px Georgia";
	ctx.fillStyle = '#000';
	ctx.fillText(player.username,player.x-ctx.measureText(player.username).width/2-cameraOffsetX,player.y-player.height/2-5-cameraOffsetY);
};


function init(){
	console.log("INIT");
	interval = setInterval(function(){update()}, 1000/60);
	map = loadMap("map1");
	platforms = map.platforms;
	audioLoop = new Audio('/music'); 
	audioLoop.addEventListener('ended', function() {
		this.currentTime = 0;
		this.play();
	}, false);
	audioLoop.play();
};

function goFullScreen(){
	if (canvas.requestFullscreen) {
	  canvas.requestFullscreen();
	} else if (canvas.msRequestFullscreen) {
	  canvas.msRequestFullscreen();
	} else if (canvas.mozRequestFullScreen) {
	  canvas.mozRequestFullScreen();
	} else if (canvas.webkitRequestFullscreen) {
	  canvas.webkitRequestFullscreen();
	}
	canvas.style.position = "absolute";
	canvas.style.top = 0;
	canvas.style.left = 0;
    canvas.width = window.innerWidth; //document.width is obsolete
    canvas.height = window.innerHeight; //document.height is obsolete
	canvas.style.width = "100%";
	canvas.style.height = "100%";
};

